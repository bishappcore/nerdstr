using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace nerdstr
{ }

public partial class Main : System.Web.UI.Page
{
    string connString = @"Data Source=nerdstr.database.windows.net;Initial Catalog=nerdstrdb;User ID=aster;Password=N@U$3*tingP@ssW0rd!;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

    protected void Page_Load(object sender, EventArgs e)


    {

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.UtcNow);
        Response.Cache.SetNoStore();
        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        //to make sure the data isn't loaded in postback
        if (!Page.IsPostBack)
        {
            PopulateGridView();
            PopulatePlayerView();
        }
    }

    void PopulateGridView()
    {
        DataTable dtbl = new DataTable();
        using (SqlConnection sqlCon = new SqlConnection(connString))
        {
            sqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM statview ORDER BY Wins DESC", sqlCon);
            sqlDa.Fill(dtbl);
        }

        if (dtbl.Rows.Count > 0)
        {
            
            StatView.DataSource = dtbl;
            StatView.DataBind();
           
        }

        else
        {
            dtbl.Rows.Add(dtbl.NewRow());
            StatView.DataSource = dtbl;
            StatView.DataBind();
            StatView.Rows[0].Cells.Clear();
            StatView.Rows[0].Cells.Add(new TableCell());
            StatView.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
            StatView.Rows[0].Cells[0].Text = "No Data Found ..!";
            StatView.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
          
        }
    }

    protected void StatView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            if (e.CommandName.Equals("AddNew"))
            {
                using (SqlConnection sqlCon = new SqlConnection(connString))
                {
                    sqlCon.Open();
                    string query = "INSERT INTO statview (Game,Faction,Player,Wins,Points)" +
                                   "VALUES (@Game,@Faction,@Player,@Wins,@Points)";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@Game", (StatView.FooterRow.FindControl("txtGameFooter") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Faction", (StatView.FooterRow.FindControl("txtFactionFooter") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Player", (StatView.FooterRow.FindControl("txtPlayerFooter") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Wins", (StatView.FooterRow.FindControl("txtWinsFooter") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Points", (StatView.FooterRow.FindControl("txtPointsFooter") as TextBox).Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    PopulateGridView();
                    lblSuccessMessage.Text = "New Player Added!";
                    lblErrorMessage.Text = "";
                }
            }
        }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

    protected void StatView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        StatView.EditIndex = e.NewEditIndex;
        PopulateGridView();
    }

    protected void StatView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        StatView.EditIndex = -1;
        PopulateGridView();
    }

    protected void StatView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
                using (SqlConnection sqlCon = new SqlConnection(connString))
                {
                    sqlCon.Open();
                string query = "UPDATE statview SET Game=@Game,Faction=@Faction,Player=@Player,Wins=@Wins,Points=@Points WHERE StatViewID = @StatViewID";
                    SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                    sqlCmd.Parameters.AddWithValue("@Game", (StatView.Rows[e.RowIndex].FindControl("txtGame") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Faction", (StatView.Rows[e.RowIndex].FindControl("txtFaction") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Player", (StatView.Rows[e.RowIndex].FindControl("txtPlayer") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Wins", (StatView.Rows[e.RowIndex].FindControl("txtWins") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@Points", (StatView.Rows[e.RowIndex].FindControl("txtPoints") as TextBox).Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@StatViewID", Convert.ToInt32(StatView.DataKeys[e.RowIndex].Value.ToString()));
                    sqlCmd.ExecuteNonQuery();
                    StatView.EditIndex = -1;
                    PopulateGridView();
                    lblSuccessMessage.Text = "Player Stats Updated!";
                    lblErrorMessage.Text = "";
                }
            
        }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

    protected void StatView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlConnection sqlCon = new SqlConnection(connString))
            {
                sqlCon.Open();
                string query = "DELETE FROM statview WHERE StatViewID = @StatViewID";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@StatViewID", Convert.ToInt32(StatView.DataKeys[e.RowIndex].Value.ToString()));
                sqlCmd.ExecuteNonQuery();
                PopulateGridView();
                lblSuccessMessage.Text = "Player Stats Deleted!";
                lblErrorMessage.Text = "";
            }

        }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

    void PopulatePlayerView()
    {
        DataTable playerdtbl = new DataTable();
        using (SqlConnection playersqlCon = new SqlConnection(connString))
        {
            playersqlCon.Open();
            SqlDataAdapter playersqlDa = new SqlDataAdapter("SELECT * FROM playerview ORDER BY Player1 DESC", playersqlCon);
            playersqlDa.Fill(playerdtbl);
        }

        if (playerdtbl.Rows.Count > 0)
        {
            PlayerView.DataSource = playerdtbl;
            PlayerView.DataBind();
        }

        else
        {
            playerdtbl.Rows.Add(playerdtbl.NewRow());
            PlayerView.DataSource = playerdtbl;
            PlayerView.DataBind();
            PlayerView.Rows[0].Cells.Clear();
            PlayerView.Rows[0].Cells.Add(new TableCell());
            PlayerView.Rows[0].Cells[0].ColumnSpan = playerdtbl.Columns.Count;
            PlayerView.Rows[0].Cells[0].Text = "No Data Found ..!";
            PlayerView.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
        }
    }

    protected void PlayerView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            if (e.CommandName.Equals("AddNew"))
            {
                using (SqlConnection playersqlCon = new SqlConnection(connString))
                {
                    playersqlCon.Open();
                    string query = "INSERT INTO playerview (Player1,Player2,MatchComplete)" +
                                   "VALUES (@Player1,@Player2,@MatchComplete)";
                    SqlCommand playersqlCmd = new SqlCommand(query, playersqlCon);
                    playersqlCmd.Parameters.AddWithValue("@Player1", (PlayerView.FooterRow.FindControl("txtPlayer1Footer") as TextBox).Text.Trim());
                    playersqlCmd.Parameters.AddWithValue("@Player2", (PlayerView.FooterRow.FindControl("txtPlayer2Footer") as TextBox).Text.Trim());
                    playersqlCmd.Parameters.AddWithValue("@MatchComplete", (PlayerView.FooterRow.FindControl("ddMatchCompleteFooter") as DropDownList).Text.Trim());
                    playersqlCmd.ExecuteNonQuery();
                    PopulatePlayerView();
                    lblSuccessMessage.Text = "Match Added!";
                    lblErrorMessage.Text = "";
                }
            }
        }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

    protected void PlayerView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        PlayerView.EditIndex = e.NewEditIndex;
        PopulatePlayerView();
    }

    protected void PlayerView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        PlayerView.EditIndex = -1;
        PopulatePlayerView();
    }

    protected void PlayerView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            using (SqlConnection playersqlCon = new SqlConnection(connString))
            {
                playersqlCon.Open();
                string query = "UPDATE playerview SET Player1=@Player1,Player2=@Player2,MatchComplete=@MatchComplete WHERE PlayerViewID = @PlayerViewID";
                SqlCommand playersqlCmd = new SqlCommand(query, playersqlCon);
                playersqlCmd.Parameters.AddWithValue("@Player1", (PlayerView.Rows[e.RowIndex].FindControl("txtPlayer1") as TextBox).Text.Trim());
                playersqlCmd.Parameters.AddWithValue("@Player2", (PlayerView.Rows[e.RowIndex].FindControl("txtPlayer2") as TextBox).Text.Trim());
                playersqlCmd.Parameters.AddWithValue("@MatchComplete", (PlayerView.Rows[e.RowIndex].FindControl("ddMatchComplete") as DropDownList).Text.Trim());
                playersqlCmd.Parameters.AddWithValue("@PlayerViewID", Convert.ToInt32(PlayerView.DataKeys[e.RowIndex].Value.ToString()));
                playersqlCmd.ExecuteNonQuery();
                PlayerView.EditIndex = -1;
                PopulatePlayerView();
                lblSuccessMessage.Text = "Matches Updated!";
                lblErrorMessage.Text = "";
            }

        }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

    protected void PlayerView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlConnection playersqlCon = new SqlConnection(connString))
            {
                playersqlCon.Open();
                string query = "DELETE FROM playerview WHERE PlayerViewID = @PlayerViewID";
                SqlCommand playersqlCmd = new SqlCommand(query, playersqlCon);
                playersqlCmd.Parameters.AddWithValue("@PlayerViewID", Convert.ToInt32(PlayerView.DataKeys[e.RowIndex].Value.ToString()));
                playersqlCmd.ExecuteNonQuery();
                PopulatePlayerView();
                lblSuccessMessage.Text = "Match Deleted!";
                lblErrorMessage.Text = "";
            }

        

    }

        catch (Exception ex)
        {
            lblSuccessMessage.Text = "";
            lblErrorMessage.Text = ex.Message;
        }
    }

}