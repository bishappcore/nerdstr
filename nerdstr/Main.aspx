﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="Main" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <!DOCTYPE html>
    <meta charset="utf-8">
    <meta name="description" content="Blog and tech tidbits by Jeremy Bishop">
    <meta name="keywords" content="Git,.net,MVC,blog">
    <meta name="author" content="Jeremy Bishop">
                       <%--   Global validation script for all fields  --%>
                              <script type="text/javascript">
                                    function ValidatorUpdateDisplay(val) {
                                             if (typeof (val.display) == "string") {
                                                     if (val.display == "None") {
                                             return;
                                         }
                                             if (val.display == "Dynamic") {
                                                val.style.display = val.isvalid ? "none" : "inline";
                                             return;
                                         }

                                         }
                                                    val.style.visibility = val.isvalid ? "hidden" : "visible";
                                             if (val.isvalid) {
                                                 document.getElementById(val.controltovalidate).style.border = '2px outset #00FF00';
                                                              }
                                             else {
                                                 document.getElementById(val.controltovalidate).style.border = '2px outset #F62817';
                                                                }          
                                           }
                              </script>    
<header id="pageHeader">
  <hgroup>
      
    <h1>nerdSTR</h1>
    <h2>Battle stats for gaming at STR</h2>
    
 </hgroup> 
</header>
  <%--<nav id="mainNav">
        <h2></h2>
        <ul>
            <li>

            </li>
        </ul>
    </nav>
      --%>
        <section id="content">
            <div class="banner">
                <h1>

                </h1>
            </div>
            </section> 
                      <article id="main">
            <h1>Current Stats</h1>
             <section>
                 
                            <asp:GridView runat="server" 
                                ID="StatView" 
                                DataKeyNames="StatViewID" 
                                AutoGenerateColumns="false" 
                                AllowPaging="true" 
                                PageSize="14"                              
                                OnRowCommand="StatView_RowCommand"
                                OnRowEditing="StatView_RowEditing"
                                OnRowCancelingEdit="StatView_RowCancelingEdit"
                                OnRowUpdating="StatView_RowUpdating"
                                OnRowDeleting="StatView_RowDeleting"
                                ShowHeaderWhenEmpty="true"
                                ShowFooter="true">
                                
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="true" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                  
                                

                                <Columns>
                               <asp:TemplateField HeaderText="Game">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Game") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtGame" Text='<%# Eval("Game") %>' runat="server" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Letters Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtGame" ValidationExpression="^[A-Za-z ]*$" >
                                       </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtGameFooter" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                              <asp:TemplateField HeaderText="Faction">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Faction") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtFaction" Text='<%# Eval("Faction") %>' runat="server" />
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only Letters Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtFaction" ValidationExpression="^[A-Za-z ]*$" >
                                      </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtFactionFooter" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                              <asp:TemplateField HeaderText="Player">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Player") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtPlayer" Text='<%# Eval("Player") %>' runat="server" />
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Only Letters Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtPlayer" ValidationExpression="^[A-Za-z ]*$" >
                                      </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtPlayerFooter" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Wins">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Wins") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtWins" Text='<%# Eval("Wins") %>' runat="server" />
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Only Numbers Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtWins" ValidationExpression="^[0-9]*$" >
                                      </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtWinsFooter" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Points">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Points") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtPoints" Text='<%# Eval("Points") %>' runat="server" />
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only Numbers Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtPoints" ValidationExpression="^[0-9]*$" >
                                      </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtPointsFooter" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:ImageButton ImageUrl="~/images/edit.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px" />
                                       <asp:ImageButton ImageUrl="~/images/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:ImageButton ImageUrl="~/images/save.png" runat="server" CommandName="Update" ToolTip="Update" Width="20px" Height="20px" />
                                       <asp:ImageButton ImageUrl="~/images/cancel.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="20px" Height="20px" />
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:ImageButton ImageUrl="~/images/addnew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                              
                           </Columns>
                            
                  </asp:GridView>
                 <br />
                 <asp:Label ID="lblSuccessMessage" Text="" runat="server" ForeColor="Green" />
                 <br />
                 <asp:Label ID="lblErrorMessage" Text="" runat="server" ForeColor="Red" />

                 <div>
                     <h1>Current Matchups</h1>      
                                             
              <asp:GridView runat="server" 
                                ID="PlayerView" 
                                DataKeyNames="PlayerViewID" 
                                AutoGenerateColumns="false" 
                                AllowPaging="true" 
                                PageSize="14"                              
                                OnRowCommand="PlayerView_RowCommand"
                                OnRowEditing="PlayerView_RowEditing"
                                OnRowCancelingEdit="PlayerView_RowCancelingEdit"
                                OnRowUpdating="PlayerView_RowUpdating"
                                OnRowDeleting="PlayerView_RowDeleting"
                                ShowHeaderWhenEmpty="true"
                                ShowFooter="true">
                                
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="true" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                  
                                

                                <Columns>
                               <asp:TemplateField HeaderText="Player 1">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Player1") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtPlayer1" Text='<%# Eval("Player1") %>' runat="server" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Only Letters Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtPlayer1" ValidationExpression="^[A-Za-z ]*$" >
                                       </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtPlayer1Footer" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                              <asp:TemplateField HeaderText="Player 2">
                                   <ItemTemplate>
                                       <asp:Label Text ='<%# Eval("Player2") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:TextBox ID="txtPlayer2" Text='<%# Eval("Player2") %>' runat="server" />
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Only Letters Allowed" Font-Size="X-Small"
                                            ControlToValidate="txtPlayer2" ValidationExpression="^[A-Za-z ]*$" >
                                      </asp:RegularExpressionValidator> 
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:TextBox ID="txtPlayer2Footer" runat="server" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Match Completed">
                                   <ItemTemplate>
                                        <asp:Label Text ='<%# Eval("MatchComplete") %>' runat="server" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:DropDownList ID="ddMatchComplete" runat="server" SelectedItem='<%# Eval("MatchComplete") %>'>
                                            <asp:ListItem Enabled="true" Text="No" Value="No" Selected="True" />
                                            <asp:ListItem Enabled="true" Text="Yes" Value="Yes" />
                                           </asp:DropDownList>
                                       
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:DropDownList ID="ddMatchCompleteFooter" runat="server" SelectItem='<%# Eval("MatchComplete") %>' >
                                           <asp:ListItem Enabled="true" Text="No" Value="No" Selected="True" />
                                       </asp:DropDownList>
                                       
                                   </FooterTemplate>
                               </asp:TemplateField>
                              
                               <asp:TemplateField>
                                   <ItemTemplate>
                                       <asp:ImageButton ImageUrl="~/images/edit.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="20px" />
                                       <asp:ImageButton ImageUrl="~/images/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="20px" />
                                   </ItemTemplate>
                                   <EditItemTemplate>
                                       <asp:ImageButton ImageUrl="~/images/save.png" runat="server" CommandName="Update" ToolTip="Update" Width="20px" Height="20px" />
                                       <asp:ImageButton ImageUrl="~/images/cancel.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="20px" Height="20px" />
                                   </EditItemTemplate>
                                   <FooterTemplate>
                                       <asp:ImageButton ImageUrl="~/images/addnew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px" />
                                   </FooterTemplate>
                               </asp:TemplateField>
                              
                           </Columns>
                            
                  </asp:GridView>
                 <br />
                 <asp:Label ID="Label1" Text="" runat="server" ForeColor="Green" />
                 <br />
                 <asp:Label ID="Label2" Text="" runat="server" ForeColor="Red" />
                 </div>
                 <div>

                 </div>
              </section>

              
        </article>        
   
        
       <article>

               <section class="newhires">
                   <p> </p>
                       

         
           </section>
       </article>
        


        <aside>
            <nav id ="archives">
                <p> </p>
            </nav>
            <section class="personal">
                
                </section>
                <section class="personal">

 
          
               </section>
        </aside>
      
     <footer id="pageFooter">

     </footer>


</asp:Content>
      
    

