﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(nerdstr.Startup))]
namespace nerdstr
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
